const boardInfo = require('./boards.json');
const getList = require("./callback2.js").getList;
const getCards = require("./callback3.js").getCards;

function getRequired(name) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const boardArr = boardInfo.find((ele) => ele.name === name);
            if (boardArr) {
                resolve(boardArr);
            } else {
                reject("data not found");
            };

        }, 2 * 1000);
    });
};
function problem5() {
    let ans = [];
    let listResult = [];

    return new Promise((resolve, reject) => {
        getRequired("Thanos").then((data) => {
            return getList(data.id);
        }).then((list) => {
            listResult = list;
            let lstID = list.find((ele) => ele.name === "Mind")
            return lstID.id;
        }).then((id) => {
            let cards = getCards(id);
            return cards
        }).then((data) => {
            ans.push(data);
            let lstID = listResult.find((ele) => ele.name === "Space")
            return lstID.id;
        }).then((id) => {
            let cards = getCards(id);
            return cards
        }).then((data) => {
            ans.push(data);
            resolve(ans)

        }).catch((err) => {
            reject(err);
        })
    })

}
module.exports.problem5 = problem5;
module.exports.getRequired = getRequired;

