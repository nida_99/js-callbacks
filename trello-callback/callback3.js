const card = require('./cards.json');

function getCards(listID) {

    return new Promise((resolve, reject) => {

        setTimeout(() => {
            let value = Object.keys(card).filter((ele) => ele === listID);
            if (value) {
                resolve(card[value]);
            } else {
                reject(err);
            };
        });
    }, 2 * 1000);
};

module.exports.getCards = getCards;


