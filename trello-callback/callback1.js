const boardInfo = require('./boards.json');

function getBoardInfo(boardID) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let value = boardInfo.find((ele) => ele.id === boardID);
            if (value) {
                resolve(value)
            } else {
                reject(err);
            }
        }, 2 * 1000);
    });
};

module.exports.getBoardInfo = getBoardInfo;
