const boardInfo = require('./boards.json');
const getList = require("./callback2.js").getList;
const getCards = require("./callback3.js").getCards;

function getRequired(name) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const boardArr = boardInfo.find((ele) => ele.name === name);
            if (boardArr) {
                resolve(boardArr);
            } else {
                reject(err);
            };

        }, 2 * 1000);
    });
};
function problem4() {

    return new Promise((resolve, reject) => {
        getRequired("Thanos").then((data) => {
            return getList(data.id);
        }).then((list) => {
            let lstID = list.find((ele) => ele.name === "Mind")
            return lstID.id;
        }).then((id) => {
            let cards = getCards(id);
            return cards
        }).then((data) => {
            resolve(data);
        }).catch((err) => {
            reject(err);
        });
    });

};
module.exports.problem4 = problem4;
module.exports.getRequired = getRequired;
