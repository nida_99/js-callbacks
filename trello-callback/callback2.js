const list = require('./lists.json');

function getList(boardID) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {

            let value = Object.keys(list).find(ele => ele === boardID)
            if (value) {
                resolve(list[value]);
            } else {
                reject("err");
            };
        });

    }, 2 * 1000);

};

module.exports.getList = getList;

