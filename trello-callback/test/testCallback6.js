const getRequired = require("../callback6.js").getRequired;
const problem6 = require("../callback6.js").problem6;
const expectedOutput = [
    [
        {
            id: '1',
            description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
        },
        {
            id: '2',
            description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
        },
        {
            id: '3',
            description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
        }
    ],
    [
        {
            id: '1',
            description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
        },
        {
            id: '2',
            description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
        },
        {
            id: '3',
            description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
        },
        {
            id: '4',
            description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
        }
    ],
    [
        {
            id: '1',
            description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
        },
        {
            id: '2',
            description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
        }
    ],
    [
        {
            id: '1',
            description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
        },
        {
            id: '2',
            description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
        }
    ],
    [
        {
            id: '1',
            description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
        },
        {
            id: '2',
            description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
        }
    ],
    undefined
];


problem6().then((data) => {
    console.log(data);
}).catch((err) => {
    console.log(err)
});
