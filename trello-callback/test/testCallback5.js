const getRequired = require("../callback5.js").getRequired;
const problem5 = require("../callback5.js").problem5;
const expectedOutput = [
  [
    {
      id: '1',
      description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    },
    {
      id: '2',
      description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    },
    {
      id: '3',
      description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    }
  ],
  [
    {
      id: '1',
      description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
    },
    {
      id: '2',
      description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
    },
    {
      id: '3',
      description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
    },
    {
      id: '4',
      description: 'intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.'
    }
  ]
];
problem5().then((data) => {

  if (JSON.stringify(data) == JSON.stringify(expectedOutput)) {
    console.log("Function is working Properly");
  } else {
    console.log("check function again")
  }

}).catch((err) => {
  console.log(err)
});


