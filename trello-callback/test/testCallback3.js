const getCards = require("../callback3.js").getCards;
const expectedOutput = [
  {
    id: '1',
    description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
  },
  {
    id: '2',
    description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
  },
  {
    id: '3',
    description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
  }
];

getCards("qwsa221").then((data) => {

  if (JSON.stringify(data) == JSON.stringify(expectedOutput)) {
    console.log("Function is working Properly");
  }
}).catch((err) => {
  console.log(err);
})
