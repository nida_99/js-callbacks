const getBoardInfo = require("../callback1.js").getBoardInfo;

getBoardInfo("mcu453ed").then((data) => {
    const expectedOutput = { id: 'mcu453ed', name: 'Thanos', permissions: {} };
    if (JSON.stringify(data) == JSON.stringify(expectedOutput)) {
        console.log("Function is working Properly");
    }
}).catch((err) => {
    console.log(err);
})
