const getList = require("../callback2.js").getList;

getList("mcu453ed").then((data) => {
    const expectedOutput = [
        { id: 'qwsa221', name: 'Mind' },
        { id: 'jwkh245', name: 'Space' },
        { id: 'azxs123', name: 'Soul' },
        { id: 'cffv432', name: 'Time' },
        { id: 'ghnb768', name: 'Power' },
        { id: 'isks839', name: 'Reality' }
    ];
    if (JSON.stringify(data) == JSON.stringify(expectedOutput)) {
        console.log("Function is working Properly");
    };

}).catch((err) => {
    if (err) {
        console.log(err);
    };
});
