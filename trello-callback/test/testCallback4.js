const getRequired = require("../callback4.js").getRequired;
const problem4 = require("../callback4.js").problem4;
const expectedOutput = [
  {
    id: '1',
    description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
  },
  {
    id: '2',
    description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
  },
  {
    id: '3',
    description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
  }
];


problem4().then((data) => {
  if (JSON.stringify(data) == JSON.stringify(expectedOutput)) {
    console.log("Function is working Properly");
  } else {
    console.log("check function again")
  }

}).catch((err) => {
  console.log(err)
});
