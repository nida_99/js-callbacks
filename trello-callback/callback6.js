const boardInfo = require('./boards.json');
const getList = require("./callback2.js").getList;
const getCards = require("./callback3.js").getCards;

function getRequired(name) {
    return new Promise((resolve, reject) => {

        setTimeout(() => {

            const boardArr = boardInfo.find((ele) => ele.name === name);
            if (boardArr) {
                resolve(boardArr);
            } else {
                reject("data not found");
            };
        }, 2 * 1000);
    });

};

function problem6() {

    return new Promise((resolve, reject) => {
        getRequired("Thanos").then((data) => {
            return getList(data.id)
        }).then((list) => {
            console.log(list)
            list.map((ele) => {
                getCards(ele.id).then((data) => {
                    console.log(data);
                });
            });

        }).catch((err) => {
            console.log(err);
        });


    });
};

module.exports.getRequired = getRequired;
module.exports.problem6 = problem6;

