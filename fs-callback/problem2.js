const fs = require('fs');
function readFile(filePath) {

    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                resolve(data);
            };
        });
    });
};


function toUpperCase(content) {
    return new Promise((resolve, reject) => {
        fs.writeFile('upperCase.txt', content.toUpperCase(), (err, data) => {
            if (err) {
                console.log(err);
            }
            else {
                fs.writeFile('filenames.txt', "upperCase.txt,", (err) => {
                    if (err) {
                        console.log(err);
                    } else {
                        resolve('upperCase.txt');
                    };

                });
            };

        });

    });
};


function toLowerCase(content) {
    return new Promise((resolve, reject) => {
        fs.readFile(content, 'utf-8', (err, upperCaseContent) => {
            if (err) {
                console.log(err);
            } else {
                let lowerCase = upperCaseContent.toLowerCase();
                resolve(lowerCase)
            };
        });
    });
};


function toSentence(lowerCaseContent) {
    return new Promise((resolve, reject) => {
        let lowerCaseText = lowerCaseContent.split('.');
        let sentencedText = lowerCaseText.join('\n');

        fs.writeFile('sentenced.txt', sentencedText, (err) => {
            if (err) {
                console.log(err);
            } else {
                fs.appendFile("filenames.txt", 'sentenced.txt,', (err) => {
                    if (err) {
                        console.log(err);
                    } else {
                        resolve(sentencedText);
                    };
                });
            };
        });

    });
};

function sortedContent(sentenceContent) {
    return new Promise((resolve, reject) => {
        let content = sentenceContent.split('\n');
        let sorted = content.sort().join('\n');

        fs.writeFile('sorted.txt', sorted, (err) => {
            if (err) {
                console.log(err);
            } else {
                fs.appendFile("filenames.txt", 'sorted.txt', (err) => {
                    if (err) {
                        console.log(err);
                    } else {
                        resolve(sorted);
                    };
                });
            };
        });
    });
};

function readFileNames(filePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                let fileNames = data.split(',');
                resolve(fileNames);
            };
        });

    });
};

function deleteFiles(fileNames) {
    return new Promise((resolve, reject) => {
        fileNames.map((ele) => {
            fs.unlink(ele, (err) => {
                if (err) {
                    console.log(err);
                };

            });
        });
        resolve();
    });
};

readFile('lipsum.txt').then((data) => {
    console.log("Read content of lipsum.txt ")
    return data;
}).then((content) => {
    console.log("converting the content to upper case ");
    return toUpperCase(content);
}).then((upperCaseContent) => {
    console.log("converting the content to lower case.");
    return toLowerCase(upperCaseContent);
}).then((lowerCaseContent) => {
    console.log('converting lower case content in sentence format.');
    return toSentence(lowerCaseContent);
}).then((sentenceText) => {
    console.log("sorting the content");
    return sortedContent(sentenceText);
}).then(() => {
    console.log("reciving the file names stored in filenames.txt");
    return readFileNames("filenames.txt");

}).then((fileNames) => {
    console.log("deleting all the files stored in filenames.txt");
    return deleteFiles(fileNames);
}).then(() => {
    console.log("Files deleted");
}).catch((err) => {
    console.log(err);
});

