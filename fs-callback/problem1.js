const fs = require('fs');

function createDirectory(filepath) {
    fs.mkdir(filepath, (err) => {
        if (err) {
            console.log(err);

        } else {
            console.log("directory created");
        };
    });

};

function addFiles(filepath, data) {
    let i = 0;
    while (i < 3) {
        let fileName = `file${i}`;
        fs.writeFile(`${filepath}/${fileName}.json`, JSON.stringify(data), (err) => {
            if (err) {
                console.log(err);
            };
        });
        i++;
    };
};

function deleteFiles(filepath) {
    let i = 0;
    while (i < 3) {
        let fileName = `file${i}`;
        fs.unlink(`${filepath}/${fileName}.json`, (err) => {
            if (err) {
                console.log(err);
            };
        });
        i++;
    };
};

const path = "callback_problems";
createDirectory(path);
const data = {
    "name": "Nida",
    "batch": "JavaScript"
}
addFiles(path, data);
deleteFiles(path)